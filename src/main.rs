fn main() {
    println!(
        "{} - Arctic Fox Portal {}",
        env!("CARGO_BIN_NAME"),
        env!("CARGO_PKG_VERSION")
    );
}
